[![pipeline status](https://gitlab.com/revington/as-circular-array/badges/master/pipeline.svg)](https://gitlab.com/revington/as-circular-array/commits/master)
[![coverage report](https://gitlab.com/revington/as-circular-array/badges/master/coverage.svg)](https://gitlab.com/revington/as-circular-array/commits/master)
# as-circular-array
Simply circular iteration over any array. 100% coverage, no deps.

## Usage

```
const circular = require('as-circular-array'),
const next = circular([1,2]);
console.log(next()); // prints 1
console.log(next()); // prints 2
console.log(next()); // prints 1
console.log(next()); // prints 2
```

## API
* `circular(array[,startIndex])` 
	* *array* the array you want to make circular
	* *index* **Optional** index to start. Default is 0.
