'use strict';
const circular = require('../index'),
    assert = require('assert');
describe('as-circular-array(array)', function () {
    describe('returned function (next)', function () {
        it('should return the next item (as in a circular array)', function () {
            var n = circular([1, 2]);
            var actual = [n(), n(), n(), n()];
            var expected = [1, 2, 1, 2];
            assert.deepEqual(actual, expected);
        });
    });
});
