'use strict';

function asCircularArray(array, index = 0) {
	const copy = array.slice(),
		len = copy.length;
	
	return function next() {
		if (index >= len) {
			index = 0;
		}
		return copy[index++];
	};
}
exports = module.exports = asCircularArray;
